use invidious::reqwest::blocking::Client;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
   let client = Client::new(String::from("https://vid.puffyan.us"));
   let search_results = client.search(Some("q=rust programming"))?.items;
   let video = client.video("5C_HPTJg5ek", None)?;

  Ok(())
}