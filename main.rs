use adw::Application;
use std::time::Duration;
use gtk::Video;
use gtk::gio;
use std::fs::File;
use std::path::Path;
use gtk::prelude::*;
use gtk::{self, ApplicationWindow, Button, prelude::*};
use invidious::reqwest::blocking::functions::video;
mod inv;


const APP_ID: &str = "org.gtk_rs.MainEventLoop1";

fn main() {
    // Create a new application
    let app = Application::builder().application_id(APP_ID).build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    print!("{}: {}", video.title, video.author);

    // Run the application
    app.run();
}

fn build_ui(app: &Application) {
    // Create a button
    let button = Button::builder()
        .label("Press me!")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();


    let video = Video::for_file(Some(&gio::File::for_path("/home/midou/Vidéos/miui.mp4")));


    // Create a window
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .child(&button)
        .child(&video)
        .build();

    // Present window
    window.present();
}
